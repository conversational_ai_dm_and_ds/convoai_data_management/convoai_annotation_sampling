# Config to pull data from Nucleus 
import platform
from sqlite3 import ProgrammingError
import sys
import snowflake 
from snowflake.connector.pandas_tools import write_pandas
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.asymmetric import dsa
from cryptography.hazmat.backends import default_backend
import os 
import pandas as pd 
# examine the platform 
userid = os.environ.get('ZID', '')
idrsa = os.environ.get('ID_RSA_P8', '')
password = os.environ.get('ID_RSA_PASSCODE', '')
role = os.environ.get('snowflake_prod_role', '')
class snowflake_connector: 
    def __init__(self, 
                    userid:str=userid, 
                    account: str='ally.us-east-1.privatelink',
                    warehouse:str='WH_TEAM_TECH_CONVOAI_ME', 
                    database:str='TEAM_TECH_CONVOAI_P', 
                    schema:str='CORE', 
                    role:str=role, 
                    private_key:str="",
                    cursor: str=""
                    ):
        '''
        feature_presentation_method: "binary", "counts", "TF-IDF"
        data: dataframe of training data 
        '''
         
        self.userid = os.getenv('ZID') #User must set envirment var
        self.account = account
        self.warehouse = warehouse
        self.database = database 
        self.schema = schema
        self.role = os.environ.get('snowflake_prod_role', '')
        self.private_key = private_key
        self.cursor = cursor 
    def create_cursor(self): 
        pwd = password.encode()
        pkey = idrsa.replace('|','\n').encode()
        
        
        p_key = serialization.load_pem_private_key(
            pkey
            ,password = pwd
            ,backend=default_backend()
        )
        pkb = p_key.private_bytes(
        encoding=serialization.Encoding.DER,
        format=serialization.PrivateFormat.PKCS8,
        encryption_algorithm=serialization.NoEncryption(),
        )
        
        self.private_key = pkb

        conn = snowflake.connector.connect(
            user = self.userid,
            account = self.account,
            warehouse = self.warehouse,
            database = self.database,
            schema = self.schema,
            role = self.role,
            private_key = pkb
        )
        #Snowflake data select and print
        self.conn = conn 
        self.cursor = conn.cursor()
    def execute_query(self, query): 
        cur = self.conn.cursor()
        try: 
            cur.execute(query)
            # print(f"INFO: Query Sccessfully!")
        except ProgrammingError as pe:
            print(f"Programming Error: {pe}")
            self.error = 'program_error'
    
    def query_to_dataframe(self, query): 
        cur = self.conn.cursor()
        try: 
            cur.execute(query)
            df = pd.DataFrame.from_records(iter(cur), columns=[x[0] for x in cur.description])
            # print(f"INFO: Query Sccessfully - {len(df)} rows!")
            return df 
        except ProgrammingError as pe:
            print(f"Programming Error: {pe}")
            self.error = 'program_error'
        except: 
            return pd.DataFrame()
        return pd.DataFrame()
    
    def write_data(self, df, table_name): 
        success, nchunks, nrows, _ = write_pandas(self.conn, df, table_name)
        # print(success, nchunks)
    def close_cursor(self):
        self.conn.cursor.close() 
    
    