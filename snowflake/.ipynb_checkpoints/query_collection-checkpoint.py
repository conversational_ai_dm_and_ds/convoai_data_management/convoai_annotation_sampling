def base_table_query(start_date=None, end_date=None): 
    query = f'''select CONVERSATION_TURN_KEY,
                CONVERSATION_ID ,
                TURN_INDEX ,
                LOAD_DATE ,
                TIMESTAMP_OF_USER_TEXT ,
                USER_UTTERANCE ,
                BOT_RESPONSE ,
                INTENT_BUSINESS_NAME ,
                INTENT_NAME ,
                cast(TRANSFERRED_TO_LIVE_AGENT as BOOLEAN) as TRANSFERRED_TO_LIVE_AGENT, 
                REASON_FOR_TRANSFER ,
                CHANNEL ,
                CONVERSATION_LEVEL ,
                ABANDONED_REASONS ,
                TRANSFER_REASONS ,
                CONTAINED_REASONS ,
                ESCALATION_REASON ,
                ANNOTATOR ,
                REVIEWED_BY ,
                REVIEWER_AGREE_ON_FIRST ,
                ANNOTATED_RESPONSE_GROUP ,
                false as EXPECTED_ANNOTATION
              from workbench_conversational_ai.tbl_feedback_template_data_master 
              where length(conversation_id) is not null and length(conversation_id) < 70
            and transferred_to_live_agent  in (true, false) 
            --limit 10
            ;'''
    return query


def query_train_data(): 
    query = f'''
    SELECT * FROM TEAM_TECH_CONVOAI_P.CORE."tbl_dim_master_utterances"
    '''
    return query 
def query_report(): 
    query = f'''SELECT * FROM USER_SANDBOX_P.PZSW95."DAILY_REPORTING_DEPOSIT_TBL"
    
    '''
    return query
def create_table_annotation_sample_table(database, schema, table_name): 
    query = f'''create or replace TABLE {database}.{schema}.{table_name} (
            CONVERSATION_ID VARCHAR(255),
            annotator VARCHAR(255),
            cross_validation_flag  BOOLEAN,
            week_ending VARCHAR(25), 
            annotation_year VARCHAR(25)
        );
        '''
    return query

def get_base_data_query(year_num=None, week_num=None, bot_name=None): 
    query = f'''SELECT distinct
                CONVERSATION_TURN_KEY  as CONVERSATION_TURN_KEY 
                ,CONVERSATION_ID  
                ,TURN_INDEX
                ,cast(a.load_date as date) as LOAD_DATE 
                ,timestamp_of_user_text as BOT_RESPONSE_DATE 
                , USER_UTTERANCE
                ,(BOT_RESPONSE ) as BOT_RESPONSE
                ,b.INTENT_BUSINESS_NAME as BUSINESS_INTENT_NAME 
                ,name as BOT_INTENT_NAME 
                ,CONFIDENCE 
                ,transferred_to_live_agent as TRANSFER_TO_AGENT 
                ,reason_for_transfer as TRANSFER_REASON
                ,CHANNEL
                ,(est_conversation_level) as CONVERSATION_LEVEL 
                ,replace(est_if_abandoned, 'nan', '') as ABANDON_REASON 
                ,case
                    when (transferred_to_live_agent = TRUE)  
                    and ( substr(lower(USER_UTTERANCE), 1, 5) = '/oos_') then 'designed_transfer'
                    when (final_est_intent_flag = 'Est Known Intent') then 'known intent'
                    when (final_est_intent_flag = 'Est Unknown Intent') then 'unknown intent'
                    when (final_est_intent_flag = 'Escalation') then 'escalation'
                    else replace(est_if_transferred, 'nan', '')
                    end as TRANSFER_REASON2
                ,replace(est_if_contained, 'nan', '') as CONTAINED_REASON 
                ,replace(EST_ESCALATION_REASON, 'nan', '') as ESCALATION_REASON 
                ,EST_RESPONSE_ACCURACY as RESPONSE_GROUP  
                ,'' as Annotator
                ,'' as Reviewed_By
                ,'' as reviewer_agree_on_first
                ,'' as QA_CHECKS
                ,FINAL_EST_INTENTS 
                ,FINAL_EST_INTENT_FLAG 
                ,EST_RESPONSE_ACCURACY 
                ,replace(est_conversation_level_flag, 'nan', '') as EST_CONVERSATION_LEVEL_FLAG 
                , replace(est_conversation_level, 'nan', '') as EST_CONVERSATION_LEVEL 
                , replace(est_if_abandoned, 'nan', '') as EST_ABANDON_REASON    
                ,replace(est_if_transferred, 'nan', '') as EST_TRANSFER_REASON  
                ,replace(est_if_contained, 'nan', '') as Est_Contained_Reason   
                ,replace(EST_ESCALATION_REASON, 'nan', '') as EST_ESCALATION_REASON 
                , greeting_closing_intents as greeting_closing_intents  
                , '' as  User_Type
                ,'N' as Reviewed
                ,'' as EST_vs_Actual
                ,'' as Is_valid_intent
        from TEAM_TECH_CONVOAI_P.CORE.TBL_BASE_CHAT_LOG_D a
        LEFT OUTER JOIN  TEAM_TECH_CONVOAI_P.CORE.TBL_DIM_BUSINESS_INTENT_NAMES b 
        ON (a.name = b.intent)
        Where 
        year(a.load_date) =  {year_num}
        and week(a.load_date ) = {week_num}
        and bot_name = '{bot_name}'
        --and from_timestamp(load_date,'yyyy-MM-dd') < from_timestamp( '2022-11-30','yyyy-MM-dd')
        ;'''
        
    return query

#TBL_ANNOTATED_SAMPLE_SNAPSHOT contains both annotated and non annotated data
def get_TBL_ANNOTATED_SAMPLE_SNAPSHOT_query(snapshot_date=None): 
    query = f'''select 
            CONVERSATION_TURN_KEY		,
            CONVERSATION_ID		,
            TURN_INDEX		,
            LOAD_DATE		,
            BOT_RESPONSE_DATE		,
            USER_UTTERANCE		,
            BOT_RESPONSE		,
            BUSINESS_INTENT_NAME		,
            BOT_INTENT_NAME		,
            CONFIDENCE		,
            TRANSFER_TO_AGENT		,
            TRANSFER_REASON		,
            CHANNEL		,
            CONVERSATION_LEVEL		,
            ABANDON_REASON		,
            UTTERANCE_TYPE		,
            CONTAINED_REASON		,
            ESCALATION_REASON		,
            RESPONSE_GROUP		,
            ANNOTATOR		,
            REVIEWED_BY		,
            REVIEWER_AGREE_ON_FIRST		,
            QA_CHECKS		,
            FINAL_EST_INTENTS		,
            FINAL_EST_INTENT_FLAG		,
            EST_RESPONSE_ACCURACY		,
            EST_CONVERSATION_LEVEL_FLAG		,
            EST_CONVERSATION_LEVEL		,
            EST_ABANDON_REASON		,
            EST_TRANSFER_REASON		,
            EST_CONTAINED_REASON		,
            EST_ESCALATION_REASON		,
            GREETING_CLOSING_INTENTS		,
            SNAPSHOT_DATE		
            from TEAM_TECH_CONVOAI_P.CORE.TBL_ANNOTATED_SAMPLE_SNAPSHOT 
            where cast(snapshot_date as date) = cast('{snapshot_date}' as date)
            and annotator is not null;'''
        
    return query

#cross validation functions
def inter_annotator_agreement(annotation_df, Z_ids=[], primary_col='bot_intent_name'):
    annotation_list = annotation_df
    agreement_data_2 = annotation_list.loc[annotation_list.duplicated(subset=['conversation_turn_key'], keep =False),:]

    #check: are all annotators accounted for in the agreement_data_2
    annotator_list = list(agreement_data_2['annotator'].unique())
    print(f'annotator list: {annotator_list}')
    
    #uncomment this
    #if len(annotator_list) != len(Z_ids):
    #    raise Exception('initial and cross val annotator lists are mismatched')

    #pivot the agreement data to allow for annotator lookup
    agreement_data_2 = agreement_data_2.pivot(index = 'conversation_turn_key', columns='annotator',values=primary_col)

    #initialize annotator reporting df
    inter_annotator_df = pd.DataFrame()
    #initialize cols - kind of hacky but it works
    for i in range(len(annotator_list)):
        inter_annotator_df[str(annotator_list[i])] = ""

    #iterate through the annotators and score against every other annotator with some cross val values
    for i in range(len(annotator_list)): #-1):
        if i == len(annotator_list):
            inter_annotator_df.loc[str(annotator_list[i]),str(annotator_list[i])] = 1
        else:
        #agreement to self is always equal to 1
            inter_annotator_df.loc[str(annotator_list[i]),str(annotator_list[i])] = 1

            for j in range(i+1,len(annotator_list)):
                #get the conversation turn keys associated with the two annotators and remove nulls 
                cols = list([str(annotator_list[i]), str(annotator_list[j])])
                df_add = agreement_data_2[agreement_data_2[cols].notnull().all(axis=1)]

                #if the resultant data frame is not empty, compute the cohen kappa score
                if df_add.empty == False:
                    user1 = list(df_add[str(annotator_list[i])])
                    user2 = list(df_add[str(annotator_list[j])])
                    inter_score = cohen_kappa_score(user1, user2)

                    #this is kind of wrong, but if cohen kappa is nan we had perfect agreement but also 100% est chance of being in perfect agreement.
                    if math.isnan(inter_score):
                        inter_score = 1
                else:
                    inter_score = 'N/A'
                
                #add scores to the df for both sides of the diagonal
                inter_annotator_df.loc[str(annotator_list[i]),str(annotator_list[j])] = inter_score
                inter_annotator_df.loc[str(annotator_list[j]),str(annotator_list[i])] = inter_score

    #Compute the overall krippendorff alpha values on both nominal and interval metrics (mostly the same for us)
    agreement_data = annotation_list.pivot(index = 'conversation_turn_key', columns='annotator',values=primary_col)
    agreement_data = agreement_data.values.tolist()
    krip_nom = krippendorff.alpha(reliability_data=agreement_data, level_of_measurement="nominal")
    krip_int = krippendorff.alpha(reliability_data=agreement_data)

    return inter_annotator_df, krip_nom, krip_int


#--------- CROSS VAL DEDUPE LOGIC ---------#

def cross_val_dedupe(annotated_df, primary_col='bot_intent_name'):
    #input: annotation template
    #output: flag for the which duplicate row to keep in the final dataframe
    #temp table incr load -> run logic -> get flag -> 
    #table with all vals (duplicates as well and flags) -> final report table where flag = True
    #ideally this runs on incremental
    #potentially adding logic as to fully agree or not -- not necessary for now as can do with a 
    #view/join on the duplicate table

    print(f"size of target df (utterance): {annotated_df['conversation_turn_key'].nunique()}")    

    #annotated_df = annotation spreadsheet template
    annotation_list = annotated_df
    print(f"size of pre-deduped annotation sample df (utterance): {annotation_list.shape}")

    #track the number of elements requiring deduping
    agreement_data_3 = annotation_list.loc[annotation_list.duplicated(subset=['conversation_turn_key']),:]
    print(f"size of dedupe sample to remove (utterance): {agreement_data_3.shape}")

    #we can either dedupe on every col but that would get unyieldy quickly as we would need to 
    #track every annotator and the logic may break down
    #my solution is to have a primary merge key: bot_intent
    annotation_list_dedupe = annotation_list.groupby('conversation_turn_key')[primary_col].agg(lambda x: pd.Series.mode(x)[0]).to_frame()
    print(f"size of deduped annotation sample df (utterance): {annotation_list_dedupe.shape}")
    print(annotation_list_dedupe)
    
    ###add = True flag here for Carey
    annotation_list_dedupe['keep_val'] = 'True'
    annotation_list['keep_val'] = 'False'

    #join back to get annotator name (and remove duplicates where same value for turn key)
    annotation_list_dedupe2 = pd.merge(
            annotation_list_dedupe, 
            annotation_list[['conversation_turn_key', primary_col, 'annotator']], 
            on=['conversation_turn_key', primary_col], how='left')
    annotation_list_dedupe2=annotation_list_dedupe2.drop_duplicates(subset=['conversation_turn_key'])
    print(f"size of merge to get annotator sample df (utterance): {annotation_list_dedupe2.shape}")

   #merge back with original annotation table to get rest of values and make the keep val of 
   #the deduped override the original vals
    final_cross_val_df = pd.merge(annotation_list, 
                                    annotation_list_dedupe2[['conversation_turn_key', 
                                                             primary_col, 'annotator','keep_val']], 
                                  on=['conversation_turn_key', primary_col, 'annotator'], how='left')
    final_cross_val_df['keep_val'] = final_cross_val_df['keep_val_y'].fillna(final_cross_val_df['keep_val_x'])
    final_cross_val_df.drop(['keep_val_y','keep_val_x'], axis = 1)
    print(f"size of final flagged sample df (utterance): {final_cross_val_df.shape}")
    print(final_cross_val_df['keep_val'].value_counts())
    
    return final_cross_val_df
